# usage


## composer install

```
% docker run --rm -it -v $(pwd):/app composer install --ignore-platform-reqs
```

## copy .env

```
cp .env.example .env
```

## sail up

```
./vendor/bin/sail up
```

## key:gen

```
./vendor/bin/sail artisan key:gen
```

## npm install

```
% ./vendor/bin/sail npm install
```

### change .env


## npm run dev

```
% ./vendor/bin/sail npm run dev
```

## migrate



import React from 'react';
import { useForm, Head } from '@inertiajs/react';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';

import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';
import { VscComment } from "react-icons/vsc";

export default function Index({ auth, chirps }) {
  const {
    data, setData, post, processing, reset, errors,
  } = useForm({
    message: '',
  });

  const submit = (e) => {
    e.preventDefault();
    post(route('chirps.store'), { onSuccess: () => reset() });
  };

  return (
    <AuthenticatedLayout user={auth.user}>
      <Head title="Chirps" />

      <div className="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
        <form onSubmit={submit}>
          <textarea
            value={data.message}
            placeholder="What's on your mind?"
            className="block w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
            onChange={(e) => setData('message', e.target.value)}
          />
          <InputError message={errors.message} className="mt-2" />
          <PrimaryButton className="mt-4" disabled={processing}>Chirp</PrimaryButton>
        </form>

        <div className="mt-6 bg-white shadow-sm rounded-lg divide-y">
          {chirps.map((chirp) => (
            <div className="p-6 flex space-x-2">
              <VscComment className="text-xl"/>
              <div className="flex-1">
                <div className="flex justify-between items-center">
                  <div>
                    <span className="text-gray-800">{chirp.user.name}</span>
                    <small className="ml-2 text-sm text-gray-600">{new Date(chirp.created_at).toLocaleString()}</small>
                  </div>
                </div>
                <p className="mt-4 text-lg text-gray-900">{chirp.message}</p>
              </div>
            </div>
          ))}
        </div>

      </div>
    </AuthenticatedLayout>
  );
}
